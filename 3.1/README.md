{% extends "../exercisetemplate.md" %}
{% block exercise %}
{% set chap = "3.1" %}
# {{ chap }} - Triangles, Congruence Relations, SAS Hypothesis

summary...

---

#### Related Definitions

{{ beginDefinition }}<a id="segAngCongruence"></a>
  Congruence for segments and angles:
  \begin{equation*}\begin{aligned}
    \overline{AB} \cong \overline{XY} &\iff AB = XY,\text{ and}\\
    \angle ABC \cong \angle XYZ &\iff m\angle ABC \cong m\angle XYZ.
  \end{aligned}\end{equation*}
{{ endDiv }}

---

#### Practice Problems:
<table>
  <tr>
    {% for ex in ["1", "3"] %}
      <th><a href="/{{ chap }}/{{ ex }}.md"><div>{{ chap }}.{{ ex }}</div></a></th>
    {% endfor %}
  </tr>
  <tr>
    {% for ex in ["9", "11"] %}
      <th><a href="/{{ chap }}/{{ ex }}.md"><div>{{ chap }}.{{ ex }}</div></a></th>
    {% endfor %}
  </tr>
</table>

#### Homework Exercises:
<table>
  <tr>
    {% for ex in ["2", "4"] %}
      <th><a href="/{{ chap }}/{{ ex }}.md"><div>{{ chap }}.{{ ex }}</div></a></th>
    {% endfor %}
  </tr>
  <tr>
    {% for ex in ["6", "8"] %}
      <th><a href="/{{ chap }}/{{ ex }}.md"><div>{{ chap }}.{{ ex }}</div></a></th>
    {% endfor %}
  </tr>
</table>
{% endblock %}
