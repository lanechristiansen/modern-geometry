<a href="https://gitlab.com/lanechristiansen/modern-geometry/pipelines" target="_blank" rel="noopener">![BUILD](https://gitlab.com/lanechristiansen/modern-geometry/badges/master/build.svg)</a>
<a href="https://gitlab.com/lanechristiansen/modern-geometry" target="_blank" rel="noopener">![HOMEWORK](https://lanechristiansen.gitlab.io/modern-geometry/badges/homework-in%20progress-orange.svg "still working on it!")</a>
[![PROBLEMS](https://lanechristiansen.gitlab.io/modern-geometry/badges/problems%20completed-24-red.svg "24 done!")][1]
[![LICENSE](https://lanechristiansen.gitlab.io/modern-geometry/badges/license-BY--SA-blue.svg)](/LICENSE.md)
<!-- Be careful, lines 2 and 3 are written by the pre-commit hook. -->

# Solutions
## for some exercises in undergraduate geometry

---

The following pages contain my attempted solutions for some exercises from <a
href="https://books.google.com/books?id=uJo9CgAAQBAJ" target="_blank"
rel="noreferrer">College Geometry: A Discovery Approach</a>. Some of the
exercise statements (and just the parts of them that I've completed) are
paraphrased here.

If you'd like to read the content of this site offline, PDFs are available [on
the downloads page](/pdf).

See <a href="https://gitlab.com/lanechristiansen/modern-geometry"
target="_blank" rel="noopener">this site's Gitlab page</a> for more information.

---

### Table of Contents
2.3 - [Incidence Axioms for Geometry](/2.3)<br>
2.4 - [Distance, Ruler Postulate, Segments, Rays, and Angles](/2.4)

<!-- Be careful, the following line is written by the pre-commit hook. -->
[1]: /3.1/1.md
