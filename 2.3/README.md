{% extends "../exercisetemplate.md" %}
{% block exercise %}
{% set chap = "2.3" %}
# {{ chap }} - Incidence Axioms for Geometry

This chapter presents five incidence axioms (I-1&ndash;I-5) adapted from
[Hilbert's axioms](https://en.wikipedia.org/wiki/Hilbert%27s_axioms). These
axioms determine how *points*, *lines*, and *planes* interact in a *space*. The
latter four terms are left undefined, but in a sense are defined by these
axioms.

---

#### Incidence Axioms

{{ beginAxiom }}
  <b><i id="I-1" style="font-style:normal;">Axiom I-1.</i></b> Each two distinct
  points determine a line.
{{ endDiv }}

{{ beginAxiom }}
  <b><i id="I-2" style="font-style:normal;">Axiom I-2.</i></b> Three
  noncollinear points determine a plane.
{{ endDiv }}

{{ beginAxiom }}
  <b><i id="I-3" style="font-style:normal;">Axiom I-3.</i></b> If two points lie
  in a plane, then any line containing those two points lies in that plane.
{{ endDiv }}

{{ beginAxiom }}
  <b><i id="I-4" style="font-style:normal;">Axiom I-4.</i></b> If two distinct
  planes meet, their intersection is a line.
{{ endDiv }}

{{ beginAxiom }}
  <b><i id="I-5" style="font-style:normal;">Axiom I-5.</i></b> Space consists of
  at least four noncoplanar points and contains three noncollinear points. Each
  plane is a set of points of which at least three are noncollinear, and each
  line is a set of at least two distinct points.
{{ endDiv }}

---

#### Practice Problems:
<table>
  <tr>
    {% for ex in ["1", "3"] %}
      <th><a href="/{{ chap }}/{{ ex }}.md"><div>{{ chap }}.{{ ex }}</div></a></th>
    {% endfor %}
  </tr>
</table>

#### Homework Exercises:
<table>
  <tr>
    {% for ex in ["2", "4", "10"] %}
      <th><a href="/{{ chap }}/{{ ex }}.md"><div>{{ chap }}.{{ ex }}</div></a></th>
    {% endfor %}
  </tr>
</table>
{% endblock %}
