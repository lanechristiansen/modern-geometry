{% extends "../exercisetemplate.md" %}
{% block exercise %}
{% set chap = "2.5" %}
# {{ chap }} - Angle Measure and the Protractor Postulate

summary...

---

#### Metric Axioms

{{ beginAxiom }}
  <b><i id="A-1" style="font-style:normal;">Axiom A-1.</i></b> Each angle
  \(\angle ABC\) is associated with a unique real number between 0 and 180,
  called its <b>measure</b> and denoted \(m\angle ABC.\) No angle can have
  measure 0 or 180.
{{ endDiv }}

{{ beginAxiom }}
  <b><i id="A-2" style="font-style:normal;">Axiom A-2.</i></b> If \(D\) lies in
  the interior of \(\angle ABC,\) then
  \begin{equation*}
    m\angle ABD + m\angle DBC = m\angle ABC.
  \end{equation*}
  Conversely, if \(m\angle ABD + m\angle DBC = m\angle ABC,\)  then ray
  \(\overrightarrow{BD}\) passes through an interior point of \(\angle ABC.\)
{{ endDiv }}

{{ beginAxiom }}
  <b><i id="A-3" style="font-style:normal;">Axiom A-3 (the <i>Protractor
  Postulate</i>).</i></b> The set of rays \(\overrightarrow{AX}\) lying on one
  side of a given line \(\overrightarrow{AB},\) including ray
  \(\overrightarrow{AB},\) may be assigned to the entire set of real numbers
  \(x,\) \(0 \leq x < 180,\) called <b>coordinates</b>, in such a manner that
  <ol>
    <li>each ray is assigned to a unique coordinate,</li>
    <li>no two rays are assigned to the same coordinate,</li>
    <li> the coordinate of \(\overrightarrow{AB}\) is 0,</li>
    <li> if rays \(\overrightarrow{AC}\) and \(\overrightarrow{AD}\) have
    coordinates \(c\) and \(d,\) then \(m\angle CAD = |c - d|.\)</li>
  </ol>
{{ endDiv }}

{{ beginAxiom }}
  <b><i id="A-4" style="font-style:normal;">Axiom A-4.</i></b> A linear pair of
  angles is a supplementary pair.
{{ endDiv }}

#### Related Definitions and a Theorem

{{ beginDefinition }}
  A point \(D\) is an <b>interior point</b> of \(\angle ABC\) iff there exists a
  segment \(\overline{EF}\) containing \(D\) as an interior point that extends
  from one side of the angle to the other (\(E \in \overrightarrow{BA}\) and \(F
  \in \overrightarrow{BC},\) \(E \neq B,\) \(F \neq B\)).
{{ endDiv }}

{{ beginDefinition }}<a id="opposite"></a>
  If \(A{\text -}B{\text -}C\) and the union of the rays \(\overrightarrow{BA}\)
  and \(\overrightarrow{BC}\) is the line \(\overleftrightarrow{AC},\) then
  these rays are said to be <b>opposite</b>.
{{ endDiv }}

{{ beginDefinition }}<a id="linearPair"></a>
  Two angles are said to form a <b>linear pair</b> iff they have one side in
  common and the other two sides are opposite rays. We call any two angles whose
  angle measures sum to 180 a <b>supplementary pair</b>, or simply,
  <b>supplementary</b>, and two angles whose measures sum to 90,
  <b>complementary</b>.
{{ endDiv }}

{{ beginTheorem }}<a id="vertical"></a>
  <b>Theorem.</b> <i><b>Vertical angles</b>, angles having the sides of one are
  opposite the sides of the other, have equal measures.</i>
{{ endDiv }}

{{ beginDefinition }}<a id="rayBetween"></a>
  For any three rays \(\overrightarrow{BA},\) \(\overrightarrow{BD},\) and
  \(\overrightarrow{BC}\) (having the same end point), we say that ray
  \(\overrightarrow{BD}\) lies <b>between</b> rays \(\overrightarrow{BA}\) and
  \(\overrightarrow{BC},\) and we write \(\overrightarrow{BA}{\text
  -}\overrightarrow{BD}{\text -}\overrightarrow{CD},\) iff the rays are distinct
  and \(m\angle ABD + m\angle DBC = m\angle ABC.\)
{{ endDiv }}

{{ beginDefinition }}<a id="bisector"></a>
  The <b>angle bisector</b> of an angle \(\angle ABC\) is any ray
  \(\overrightarrow{BD}\) lying between the sides \(\overrightarrow{BA}\) and
  \(\overrightarrow{BC}\) such that \(m\angle ABD = m\angle DBC.\)
{{ endDiv }}

---

#### Practice Problems:
<table>
  <tr>
    {% for ex in ["1", "3"] %}
      <th><a href="/{{ chap }}/{{ ex }}.md"><div>{{ chap }}.{{ ex }}</div></a></th>
    {% endfor %}
  </tr>
</table>

#### Homework Exercises:
<table>
  <tr>
    <th colspan="2"><a href="/{{ chap }}/2.md"><div>{{ chap }}.2</div></a></th>
    <th colspan="2"><a href="/{{ chap }}/4.md"><div>{{ chap }}.4</div></a></th>
    <th colspan="2"><a href="/{{ chap }}/5.md"><div>{{ chap }}.5</div></a></th>
  </tr>
  <tr>
    <th colspan="3"><a href="/{{ chap }}/8.md"><div>{{ chap }}.8</div></a></th>
    <th colspan="3"><a href="/{{ chap }}/9.md"><div>{{ chap }}.9</div></a></th>
  </tr>
</table>
{% endblock %}
