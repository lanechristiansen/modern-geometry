{% extends "../exercisetemplate.md" %}
{% block exercise %}
{% set chap = "2.4" %}
# {{ chap }} - Distance, Ruler Postulate, Segments, Rays, and Angles

This chapter covers the metric axioms, which define a *distance* between points
that is nonnegative, positive for distinct points, and ignores the order of the
points. The fourth metric axiom, which is not an axiom of certain geometries,
establishes a correspondence between the points on a line and the real numbers.

This chapter closely investigates *betweenness,* which is used to convey the
order of collinear points.

---

#### Metric Axioms

{{ beginAxiom }}
  <b><i id="D-1" style="font-style:normal;">Axiom D-1.</i></b> Each pair of
  points \(A\) and \(B\) is associated with a unique real number, called the
  <b>distance</b> from \(A\) to \(B\), denoted by \(AB.\)
{{ endDiv }}

{{ beginAxiom }}
  <b><i id="D-2" style="font-style:normal;">Axiom D-2.</i></b> For all points
  \(A\) and \(B,\) \(AB \geq 0,\) with equality only when \(A = B.\)
{{ endDiv }}

{{ beginAxiom }}
  <b><i id="D-3" style="font-style:normal;">Axiom D-3.</i></b> For all points
  \(A\) and \(B,\) \(AB = BA.\)
{{ endDiv }}

{{ beginAxiom }}
  <b><i id="D-4" style="font-style:normal;">Axiom D-4 (the Ruler
  Postulate).</i></b> The points of each line \(l\) may be assigned to the
  entire set of real numbers \(x,\) \(-\infty < x < \infty,\) called
  <b>coordinates</b>, in such a manner that
  <ol>
    <li>each point on \(l\) is assigned to a unique coordinate,</li>
    <li>no two points are assigned to the same coordinate,</li>
    <li>any two points on \(l\) may be assigned the coordinates zero and a
    positive real number, respectively, and</li>
    <li>if points \(A\) and \(B\) on \(l\) have coordinates \(a\) and \(b,\)
    then \(AB = |a - b|.\)</li>
  </ol>
{{ endDiv }}

#### Related Definitions and a Theorem

{{ beginTheorem }}
  <b>Theorem (the Triangle Inequality).</b> <i>If \(A,\) \(B,\) and \(C\) are
  any three distinct points, then \(AB + BC \geq AC,\) with equality only when
  the points are collinear, and \(A{\text -}B{\text -}C.\)</i>
{{ endDiv }}

{{ beginDefinition }}<a id="between"></a>
  For any three points \(A,\) \(B,\) and \(C,\) we say that \(B\) is
  <b>between</b> \(A\) and \(C,\) and we write \(A{\text -}B{\text -}C,\) iff
  \(A,\) \(B,\) and \(C\) are distinct, collinear points, and \(AB + BC = AC.\)
{{ endDiv }}

{{ beginDefinition }}<a id="quadBetween"></a>
  If \(A,\) \(B,\) \(C,\) and \(D\) are four distinct collinear points, then we
  write \(A{\text -}B{\text -}C{\text -}D\) iff the composite of all four
  betweenness relations \(A{\text -}B{\text -}C,\) \(B{\text -}C{\text -}D,\)
  \(A{\text -}B{\text -}D,\) and \(A{\text -}C{\text -}D\) are true.
{{ endDiv }}

---

#### Practice Problems:
<table>
  <tr>
    <th colspan="2"><a href="/{{ chap }}/1.md"><div>{{ chap }}.1</div></a></th>
    <th colspan="2"><a href="/{{ chap }}/3.md"><div>{{ chap }}.3</div></a></th>
    <th colspan="2"><a href="/{{ chap }}/7.md"><div>{{ chap }}.7</div></a></th>
  </tr>
  <tr>
    <th colspan="3"><a href="/{{ chap }}/9.md"><div>{{ chap }}.9</div></a></th>
    <th colspan="3"><a href="/{{ chap }}/11.md"><div>{{ chap }}.11</div></a></th>
  </tr>
</table>

#### Homework Exercises:
<table>
  <tr>
    {% for ex in ["2", "8", "10"] %}
      <th><a href="/{{ chap }}/{{ ex }}.md"><div>{{ chap }}.{{ ex }}</div></a></th>
    {% endfor %}
  </tr>
  <tr>
    {% for ex in ["12", "13", "14"] %}
      <th><a href="/{{ chap }}/{{ ex }}.md"><div>{{ chap }}.{{ ex }}</div></a></th>
    {% endfor %}
  </tr>
</table>
{% endblock %}
