# [Solutions for some exercises in undergraduate geometry][1]

This is the repository supporting a [GitLab pages site][1] that contains some of
my work done for an undergraduate geometry course. Copyright 2018 Lane
Christiansen, [license info is here.][2]

---

To run this stuff yourself, you'll need to:
```
# clone this stuff somewhere nice
$ git clone git@gitlab.com:lanechristiansen/modern-geometry.git .
```
```
# install gitbook
$ npm install gitbook-cli -g
```
```
# install necessary plugins (see book.json)
$ gitbook install
```
```
# serve the site locally
$ gitbook serve
```
Although this is built with [Gitbook](https://www.gitbook.com/), which is
capable of building PDF, ePub, etc., these don't play well with
[KaTeX](https://github.com/Khan/KaTeX), which the website depends on. However,
PDFs containing the content of the site are available [on the downloads
page](/pdf). If you have LaTeX installed, these can be built by:
```
cd pdf
latexmk -pdf portfolio.tex
sed -n '1,2p' portfolio.tex > portfolio-bw.tex
echo -n '% ' >> portfolio-bw.tex
sed -n '3,$p' portfolio.tex >> portfolio-bw.tex
latexmk -pdf portfolio-bw.tex
```
This produces two versions of the document, one with and one without colored
links.

---

Please update the Google Analytics information in `book.json`, or remove it, if
this page is hosted elsewhere.

If you'd like to use the badge-updating pre-commit script, which depends on some
POSIX utilities, do
```
cp pre-commit .git/hooks/
```

[1]: https://lanechristiansen.gitlab.io/modern-geometry/
[2]: https://gitlab.com/lanechristiansen/modern-geometry/blob/master/LICENSE.md
