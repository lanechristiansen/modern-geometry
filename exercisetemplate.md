{% set hugeCentered = "<img style=\"width: 100%; max-width: 450px; margin: auto; display: block;\" src=\"" %}
{% set bigCentered = "<img style=\"width: 100%; max-width: 375px; margin: auto; display: block;\" src=\"" %}
{% set medCentered = "<img style=\"width: 100%; max-width: 250px; margin: auto; display: block;\" src=\"" %}
{% set smCentered = "<img style=\"width: 100%; max-width: 150px; margin: auto; display: block;\" src=\"" %}
{% set endImg = "\">" %}
{% set conclusionsJustifications = "<tr>
  <td><span style=\"font-variant: small-caps\"><b>conclusions</b></span></td>
  <td><span style=\"font-variant: small-caps\"><b>justifications</b></span></td>
</tr>" %}
{% set showGeogebra3dRotateOnly = "<script type=\"text/javascript\">
  loadScript(\"https://cdn.geogebra.org/apps/deployggb.js\", function() {
    var ggbs = document.getElementsByClassName(\"ggb\");
    for (var i = 0; i < ggbs.length; i++) {
      var ggbApp = new GGBApplet({\"appName\": \"3d\",
        \"material_id\": ggbs[i].textContent, \"showResetIcon\": true,
        \"enableRightClick\": false, \"showFullscreenButton\": true,
        \"appletOnLoad\": function() {ggbApplet.setMode(540);}}, true);
      ggbs[i].innerHTML = \"\";
      ggbApp.inject(ggbs[i].id);
    }
  })
</script>" %}
{% set showGeogebraGeometry = "<script type=\"text/javascript\">
  loadScript(\"https://cdn.geogebra.org/apps/deployggb.js\", function() {
    var ggbs = document.getElementsByClassName(\"ggb\");
    for (var i = 0; i < ggbs.length; i++) {
      var ggbApp = new GGBApplet({\"appName\": \"geometry\",
        \"material_id\": ggbs[i].textContent, \"showResetIcon\": true,
        \"enableRightClick\": false, \"showFullscreenButton\": true,
        \"appletOnLoad\": function() {ggbApplet.setMode(0);}}, true);
      ggbs[i].innerHTML = \"\";
      ggbApp.inject(ggbs[i].id);
    }
  })
</script>" %}
{% set beginAxiom = "<div class=\"axiom\">" %}
{% set beginTheorem = "<div class=\"theorem\">" %}
{% set beginLemma = "<div class=\"lemma\">" %}
{% set beginProposition = "<div class=\"proposition\">" %}
{% set beginProof = "<div class=\"proof\">" %}
{% set beginPartProof = "<div class=\"partProof\">" %}
{% set beginDefinition = "<div class=\"definition\">" %}
{% set endDiv = "</div>" %}
{% block exercise %}{% endblock %}
