{% extends "../exercisetemplate.md" %}
{% block exercise %}
{% set chap = "3.3" %}
# {{ chap }} - SAS, ASA, SSS Congruence and Perpendicular Bisectors

summary...

---

axioms, thms...

---

#### Practice Problems:
<table>
  <tr>
    <th colspan="2"><a href="/{{ chap }}/1.md"><div>{{ chap }}.1</div></a></th>
    <th colspan="2"><a href="/{{ chap }}/3.md"><div>{{ chap }}.3</div></a></th>
    <th colspan="2"><a href="/{{ chap }}/5.md"><div>{{ chap }}.5</div></a></th>
  </tr>
  <tr>
    <th colspan="3"><a href="/{{ chap }}/7.md"><div>{{ chap }}.7</div></a></th>
    <th colspan="3"><a href="/{{ chap }}/9.md"><div>{{ chap }}.9</div></a></th>
  </tr>
</table>

#### Homework Exercises:
<table>
  <tr>
    <th colspan="2"><a href="/{{ chap }}/6.md"><div>{{ chap }}.6</div></a></th>
    <th colspan="2"><a href="/{{ chap }}/8.md"><div>{{ chap }}.8</div></a></th>
    <th colspan="2"><a href="/{{ chap }}/10.md"><div>{{ chap }}.10</div></a></th>
  </tr>
  <tr>
    <th colspan="3"><a href="/{{ chap }}/12.md"><div>{{ chap }}.12</div></a></th>
    <th colspan="3"><a href="/{{ chap }}/20.md"><div>{{ chap }}.20</div></a></th>
  </tr>
</table>
{% endblock %}
