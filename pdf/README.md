# Downloads

PDFs of the content of this site are available in two forms: one
[with colored
links](https://lanechristiansen.gitlab.io/modern-geometry/pdf/portfolio.pdf),
and one more suitable for printing, [in black and white
only](https://lanechristiansen.gitlab.io/modern-geometry/pdf/portfolio-bw.pdf).
