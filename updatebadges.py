#!/usr/bin/python3
import sys
import os
import tkinter as tk
import linecache
import re
import fileinput
import subprocess

# Updates badges and their links.

try:
    def problemsPreviouslyCompleted():
        line = linecache.getline('INDEX.md', 3)
        matchObj = re.match(r".*?completed-(.*)-.*", line)
        if matchObj:
            return matchObj.group(1)
        else:
            raise Exception()

    def lastChapterAndExercise():
        chap = 1
        ex = 1
        line = linecache.getline('latest.html', 4)
        chapMatch = re.match(r".*?geometry/(.*).html.*", line)
        if chapMatch:
            chap, ex = chapMatch.group(1).split('/')
        else:
            raise Exception()
        return chap, ex

    def linesInIndex():
        def file_len(fname):
            p = subprocess.Popen(['wc', '-l', fname], stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
            result, err = p.communicate()
            if p.returncode != 0:
                raise IOError(err)
            return int(result.strip().split()[0])
        return file_len('INDEX.md')

    root = tk.Tk()

    #######################################
    # number of problems completed
    tk.Label(root,
             text="""Total problems completed:""",
             justify = tk.LEFT,
             padx = 20).pack()

    n = tk.Scale(root, from_=0, to=100, length=400, tickinterval=10,
                    orient=tk.HORIZONTAL)
    n.set(problemsPreviouslyCompleted())
    n.pack()

    #######################################
    # last problem chapter and number
    chap, ex = lastChapterAndExercise()
    tk.Label(root,
             text="""Last problem completed:""",
             justify = tk.LEFT,
             pady = 10).pack()
    top = tk.Frame(root)
    bottom = tk.Frame(root)
    top.pack(side = tk.TOP)
    bottom.pack(side = tk.BOTTOM, fill = tk.BOTH, expand = True)
    tk.Label(root, text = "Chapter:", padx = 10).pack(in_ = top, side = tk.LEFT)
    chapEntry = tk.StringVar()
    chapBox = tk.Entry(root, width = 5, textvariable = chapEntry)
    chapEntry.set(chap)
    chapBox.pack(in_ = top, side = tk.LEFT)
    tk.Label(root, text = "Problem:", padx = 10).pack(in_ = top, side = tk.LEFT)
    exEntry = tk.StringVar()
    exBox = tk.Entry(root, width = 5, textvariable = exEntry)
    exEntry.set(ex)
    exBox.pack(in_ = top, side = tk.LEFT)

    #######################################
    # homework: submitted, in progress, or complete
    statusvar = tk.IntVar()
    statusvar.set(0)

    statuses = ["in progress", "submitted", "complete"]

    tk.Label(root,
             text="""Homework:""",
             justify = tk.LEFT,
             pady = 10).pack()
    top2 = tk.Frame(root)
    bottom2 = tk.Frame(root)
    top2.pack(side=tk.TOP)
    bottom2.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

    for val, status in enumerate(statuses):
        tk.Radiobutton(root,
            text=status,
            indicatoron = 0,
            width = 10,
            padx = 10,
            variable=statusvar,
            value=val).pack(anchor=tk.W, in_=top2, side=tk.BOTTOM)

    ########################################
    # button:
    def output():
        # do the status badge
        indexS = """<a href="https://gitlab.com/lanechristiansen/modern-geometry" target="_blank" rel="noopener">![HOMEWORK](https://lanechristiansen.gitlab.io/modern-geometry/badges/homework-{}")</a>\n"""
        if statusvar.get() == 0:
            progress = """in%20progress-orange.svg "still working on it!"""
            progressFile = 'homework-in progress-orange.svg'
            indexS = indexS.format(progress)
        elif statusvar.get() == 1:
            progress = """submitted-green.svg "homework submitted!"""
            progressFile = 'homework-submitted-green.svg'
            indexS = indexS.format(progress)
        elif statusvar.get() == 2:
            progress = """done-brightgreen.svg "done!"""
            progressFile = 'homework-done-brightgreen.svg'
            indexS = indexS.format(progress)
        os.system("""cp badges/"{}" badges/progress.svg""".format(progressFile))

        # do the problems badge
        # first build the filename
        color = ''
        if n.get() <= 35:
            color = 'red'
        elif n.get() <= 60:
            color = 'orange'
        elif n.get() <= 70:
            color = 'yellow'
        elif n.get() <= 80:
            color = 'yellowgreen'
        elif n.get() <= 90:
            color = 'green'
        else:
            color = 'brightgreen'
        badgefile = "badges/problems completed-{}-{}.svg".format(str(n.get()), color)
        badgeurl = "https://img.shields.io/badge/problems%20completed-{}-{}.svg".format(str(n.get()), color)
        indexbadgestring = """[![PROBLEMS](https://lanechristiansen.gitlab.io/modern-geometry/badges/problems%20completed-{}-{}.svg "{} done!")][1]""".format(str(n.get()), color, str(n.get()))
        # check to see if the file's there
        # if not, download it
        if not os.path.exists(badgefile):
            os.system("wget -P 'badges' {}".format(badgeurl))
        if not os.path.exists(badgefile):
            print('couldn\'t get the badge from shields.io!')
            raise Exception()
        os.system("""cp "{}" badges/latest.svg""".format(badgefile))

        # remove other problem badges
        os.chdir('badges')
        rmSystemCall = """ls | grep "^problems completed" | grep -v "{}" | xargs -d"\n" rm"""
        os.system(rmSystemCall.format(n.get()))
        os.chdir('..')
        os.system('git add \"{}\"'.format(badgefile))

        # then write the markdown
        indexS += indexbadgestring
        indexSystemCall = """{{ sed -n '1,1p' INDEX.md; echo '{}'; sed -n '4,$p' INDEX.md; }} > INDEX.md.tmp && mv INDEX.md.tmp INDEX.md"""
        os.system(indexSystemCall.format(indexS))
        indexLines = linesInIndex()
        indexSystemCall = """{{ sed -n '1,{}p' INDEX.md; echo '{}'; }} > INDEX.md.tmp && mv INDEX.md.tmp INDEX.md"""
        os.system(indexSystemCall.format(int(indexLines) - 1, '[1]: /{}/{}.md'.format(chapEntry.get(), exEntry.get())))
        latestString = """<meta http-equiv="refresh" content="0; URL=https://lanechristiansen.gitlab.io/modern-geometry/{}/{}.html" />"""
        latestString = latestString.format(chapEntry.get(), exEntry.get())
        latestSystemCall = """{{ sed -n '1,3p' latest.html; echo '{}'; sed -n '5,$p' latest.html; }} > latest.html.tmp && mv latest.html.tmp latest.html"""
        os.system(latestSystemCall.format(latestString))
        os.system('git add latest.html')
        sys.exit(0)

    button = tk.Button(root, text='commit!', width=15, command=output)
    button.pack()
    root.mainloop()
except Exception as e:
    if hasattr(e, 'message'):
        print(e.message)
    else:
        print(e)
    print('an error occurred, the push won\'t be made')
    sys.exit(1)
